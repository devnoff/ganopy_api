/*
 * Including
 */
var ProductModel = require('../model/product');




/*
 * Class Declaration
 */

function Product() {};



/*
 * Routing
 */

var product = new Product();

module.exports.route = function(server) {
	server.post('/product', product.setProduct);
	server.get('/product/:id', product.getProduct);
	server.post('/product/:id/like', product.likeProduct);
	server.del('/product/:id/like', product.unlikeProduct);
	server.post('/product/:id/collect', product.collectProduct);
	server.del('/product/:id/collect', product.uncollectProduct);
	server.get('/product/:id/likes', product.getProductLikes);
	server.get('/product/:id/comments', product.getProductComments);
	server.get('/products', product.getProducts);
	server.get('/product/feed', product.getProductFeed);
}


Product.prototype.setProduct = function(req, res, next) {

	var productModel = new ProductModel();

	res.send(req.body);

}


Product.prototype.getProduct = function(req, res, next) {

	var productModel = new ProductModel();



}


Product.prototype.likeProduct = function(req, res, next) {

};


Product.prototype.unlikeProduct = function(req, res, next) {

};


Product.prototype.collectProduct = function(req, res, next) {

};


Product.prototype.uncollectProduct = function(req, res, next) {

};


Product.prototype.getProductLikes = function(req, res, next) {

};


Product.prototype.getProductComments = function(req, res, next) {

};


Product.prototype.getProducts = function(req, res, next) {

};


Product.prototype.getProductFeed = function(req, res, next) {

};

