var restify = require('restify');


function respond(req, res, next) {
  res.send({result: 'hello ' + req.params.name});
  next();
}

var server = restify.createServer();

server.use(restify.acceptParser(server.acceptable));
server.use(restify.authorizationParser());
server.use(restify.dateParser());
server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(restify.gzipResponse());


// load route
require('./route.js')(__dirname+'/controller', server);

server.listen(8080, function() {
  console.log('%s listening at %s', server.name, server.url);
});