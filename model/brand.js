// category model
var mongoose = require('mongoose'),
Schema = mongoose.Schema;

var brandSchema = new Schema({
	name: String,
	profileimage: String
});

module.exports = mongoose.model('Brand', brandSchema);
