var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/ganopy');
var Schema = mongoose.Schema;


//define schemes

var likeSchema = new Schema({
  user : { type: Schema.Types.ObjectId, ref: 'User' },
  date : { type : Date, default: Date.now }
});

var commentSchema = new Schema({
  user : { type: Schema.Types.ObjectId, ref: 'User' },
  content : { type : String },
  date : { type : Date, default: Date.now },
});

var productSchema = new Schema({
  productid: String,
  name: String,
  coverimage: String,
  categories: [
    { type: Schema.Types.ObjectId, ref: 'Category' }
  ],
  brand: { type: Schema.Types.ObjectId, ref: 'Brand' },
  price: {
    amount: Number,
    unit: String,
    unit_symbol: String,
    formatted: String
  },
  original_url: String,
  liked: Boolean,
  collected: Boolean,
  likes: [likeSchema],
  likes: [commentSchema]
});


module.exports = mongoose.model('Product', productSchema);